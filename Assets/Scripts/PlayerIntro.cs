﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerIntro : MonoBehaviour
{
  public GameController gameController;
  public GameObject train;

  float speed = 1.0f;
  float endPos = -4.5f;
  float stoppingDistance = 0.4f;

  private void Update()
  {
    switch (gameController.gameState)
    {
      case GameState.MONOLOGUE:
        FollowTrain();
        break;
      case GameState.COMING_IN:
        WalkToDoor();
        break;
      case GameState.GAME_CONTROL:
        LeaveTrain();
        break;
      default:
        break;
    }
  }

  void FollowTrain()
  {
    transform.position = new Vector3
    (
      train.transform.position.x + 0.5f,
      transform.position.y,
      train.transform.position.z
    );
  }

  void WalkToDoor()
  {
    transform.position = new Vector3
    (
      train.transform.position.x + 0.5f,
      transform.position.y,
      Mathf.Lerp(transform.position.z, transform.position.z - 1f, Time.deltaTime * speed)
    );
  }

  void LeaveTrain()
  {
    transform.position = new Vector3
    (
      transform.position.x,
      transform.position.y,
      Mathf.Lerp(transform.position.z, endPos, Time.deltaTime * speed)
    );

    if (Mathf.Abs(transform.position.z - endPos) <= stoppingDistance)
    {
      gameController.gameState = GameState.PLAYER_CONTROL;
    }
  }
}
