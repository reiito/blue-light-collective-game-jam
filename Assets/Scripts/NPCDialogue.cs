﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Yarn.Unity;

public class NPCDialogue : MonoBehaviour
{
  public string treeID;

  void Talk()
  {
    // dialogue stuff here
    FindObjectOfType<DialogueRunner>().startNode = treeID;
    FindObjectOfType<DialogueRunner>().StartDialogue();
  }
}
