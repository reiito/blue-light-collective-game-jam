﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallColor : MonoBehaviour
{
  public GameObject wall;
  public GameObject[] doors = new GameObject[2];
  public GameObject[] windows = new GameObject[5];

  public bool start { get; set; }

  Color[] newColors = new Color[4];
  float speed = 7.0f;

  int initRenderQueue = 3000;

  private void Awake()
  {
    SetRenderQues();

    for (int i = 0; i < wall.GetComponent<Renderer>().materials.Length; i++)
    {
      newColors[i] = new Color(wall.GetComponent<Renderer>().materials[i].color.r, wall.GetComponent<Renderer>().materials[i].color.g, wall.GetComponent<Renderer>().materials[i].color.b, 1);
    }

    newColors[2] = new Color(doors[0].GetComponent<Renderer>().material.color.r, doors[0].GetComponent<Renderer>().material.color.g, doors[0].GetComponent<Renderer>().material.color.b, 1);
    newColors[3] = new Color(windows[0].GetComponent<Renderer>().material.color.r, windows[0].GetComponent<Renderer>().material.color.g, windows[0].GetComponent<Renderer>().material.color.b, 0.5f);
  }

  private void Update()
  {
    if (start)
    {
      wall.GetComponent<Renderer>().materials[0].color = Color.Lerp(wall.GetComponent<Renderer>().materials[0].color, newColors[0], Time.deltaTime * speed);
      wall.GetComponent<Renderer>().materials[1].color = Color.Lerp(wall.GetComponent<Renderer>().materials[1].color, newColors[1], Time.deltaTime * speed);

      for (int i = 0; i < doors.Length; i++)
      {
        doors[i].GetComponent<Renderer>().material.color = Color.Lerp(doors[i].GetComponent<Renderer>().material.color, newColors[2], Time.deltaTime * speed);
      }

      for (int i = 0; i < windows.Length; i++)
      {
        windows[i].GetComponent<Renderer>().material.color = Color.Lerp(windows[i].GetComponent<Renderer>().material.color, newColors[3], Time.deltaTime * speed);
      }

      if (wall.GetComponent<Renderer>().materials[0].color.a >= 0.9f)
      {
        SetFinalMaterials();
      }
    }
  }

  void SetRenderQues()
  {
    for (int i = 0; i < windows.Length; i++)
    {
      windows[i].GetComponent<Renderer>().material.renderQueue = initRenderQueue;
    }

    for (int i = 0; i < doors.Length; i++)
    {
      doors[i].GetComponent<Renderer>().material.renderQueue = initRenderQueue + 1;
    }

    wall.GetComponent<Renderer>().materials[1].renderQueue = initRenderQueue + 1;

    wall.GetComponent<Renderer>().materials[0].renderQueue = initRenderQueue + 3;
  }

  void SetFinalMaterials()
  {
    for (int i = 0; i < wall.GetComponent<Renderer>().materials.Length; i++)
    {
      wall.GetComponent<Renderer>().materials[i].color = newColors[i];
      StandardShaderUtils.ChangeRenderMode(wall.GetComponent<Renderer>().materials[i], StandardShaderUtils.BlendMode.Opaque);
    }

    for (int i = 0; i < doors.Length; i++)
    {
      doors[i].GetComponent<Renderer>().material.color = newColors[2];
      StandardShaderUtils.ChangeRenderMode(doors[i].GetComponent<Renderer>().material, StandardShaderUtils.BlendMode.Opaque);
    }

    for (int i = 0; i < windows.Length; i++)
    {
      windows[i].GetComponent<Renderer>().material.color = newColors[3];
      StandardShaderUtils.ChangeRenderMode(windows[i].GetComponent<Renderer>().material, StandardShaderUtils.BlendMode.Transparent);
    }
  }
}
