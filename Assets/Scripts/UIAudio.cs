﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIAudio : MonoBehaviour
{
  private AudioSource _source;

  public AudioClip typingSound;
  private float volLowRange = -1.5f;
  private float volHighRange = 1.0f;

  void Awake()
  {
    _source = GetComponent<AudioSource>();

    if (_source == null)
    {
      _source = gameObject.AddComponent<AudioSource>();
    }
  }

  // Use this for initialization
  // void Start ()
  // {
  // if (clip != null)
  // {
  //   _source.clip = clip;
  // }
  // }

  // Update is called once per frame
  void Update()
  {

  }

  public void PlayTypingSound()
  {
    float vol = Mathf.Clamp(Random.Range(volLowRange, volHighRange), 0.0f, 0.5f);
    _source.PlayOneShot(typingSound, vol);
  }
}
